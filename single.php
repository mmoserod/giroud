<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div class="search-form">
	<div class="row">
		<div class="column">
				<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
				    <label>
				        <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
				        <input type="search" class="search-field"
				            placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>"
				            value="<?php echo get_search_query() ?>" name="s"
				            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
				    </label>
				    <input type="submit" class="search-submit"
				        value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
				</form>
		</div>
	</div>
</div>

<div class="row">

	<div class="large-9 columns">
		<?php do_action( 'foundationpress_before_content' ); ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">

				<?php do_action( 'foundationpress_post_before_entry_content' ); ?>
				<div class="entry-content">

		    	<?php if ( wp_attachment_is_image( $post->id ) ) : $att_image = wp_get_attachment_image_src( $post->id, "large"); ?>
		    	<?php $att_large = wp_get_attachment_image_src( $post->id, "large"); ?>
		    	
		    	<img src="<?php echo $att_image[0];?>" width="<?php echo $att_image[1];?>" height="<?php echo $att_image[2];?>"  class="attachment-medium" alt="<?php $post->post_excerpt; ?>" />

		    	<?php else : ?>
		    	    <a href="<?php echo wp_get_attachment_url($post->ID) ?>" title="<?php echo wp_specialchars( get_the_title($post->ID), 1 ) ?>" rel="attachment"><?php echo basename($post->guid) ?></a>
		    	<?php endif; ?>

				
				</div>




			</article>
		<?php endwhile;?>

		<?php do_action( 'foundationpress_after_content' ); ?>
		
	</div>

	<?php get_sidebar(); ?>

</div>

<div class="row">
	<div class="column">
		 <h4>More Images: </h4>
		<div class="row large-up-6">
		<?php

		$currentID = get_the_ID();


		$args = array(
		   'post_type' => 'attachment',
		   'numberposts' => 6,
		   'post_parent' => $post->post_parent,
		   'post__not_in' => array($currentID),
		   'orderby' => 'rand',
		   'post_status' => null
		  );

		  $attachments = get_posts( $args );
		     if ( $attachments ) {
		        foreach ( $attachments as $attachment ) {
		           echo '<div class="column">';
		           echo '<a class="th" title="';
		           echo apply_filters( 'the_title', $attachment->post_title );
		           echo '" href="';
		           echo 'http://productivity.mmoser.com/igallery/', '?attachment_id='.$attachment->ID;
		           echo '">';
		           echo wp_get_attachment_image( $attachment->ID, 'thumbnail' );
		           echo '</a>';
		           echo '</div>';
		          }
		     }
		 ?>
		</div>
	</div>
</div>

<?php get_footer();
