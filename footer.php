<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

		</section>

		<footer id="footer">
			<div class="row">
				<div class="column">
					<p class="text-center"><small>All images copyrighted by &copy; M Moser Associates 2017 <br>
					<a href="http://www.mmoser.com">www.mmoser.com </a></small></p>
				</div>
			</div>
		</footer>

		    </div><!-- off-canvas-content -->
		  </div><!-- off-canvas-wrapper-inner -->
		</div><!-- off-canvas-wrapper -->


		<?php do_action( 'foundationpress_layout_end' ); ?>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) == 'offcanvas' ) : ?>
		</div><!-- Close off-canvas wrapper inner -->
	</div><!-- Close off-canvas wrapper -->
</div><!-- Close off-canvas content wrapper -->
<?php endif; ?>


<?php wp_footer(); ?>
<a href="javascript:void(0);" class="back-to-top">
    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="72" height="84" viewBox="0 0 24 28"><path d="M20.062 13.984c0-0.266-0.094-0.516-0.281-0.703l-7.078-7.078c-0.187-0.187-0.438-0.281-0.703-0.281s-0.516 0.094-0.703 0.281l-7.078 7.078c-0.187 0.187-0.281 0.438-0.281 0.703s0.094 0.516 0.281 0.703l1.422 1.422c0.187 0.187 0.438 0.281 0.703 0.281s0.516-0.094 0.703-0.281l2.953-2.953v7.844c0 0.547 0.453 1 1 1h2c0.547 0 1-0.453 1-1v-7.844l2.953 2.953c0.187 0.187 0.438 0.297 0.703 0.297s0.516-0.109 0.703-0.297l1.422-1.422c0.187-0.187 0.281-0.438 0.281-0.703zM24 14c0 6.625-5.375 12-12 12s-12-5.375-12-12 5.375-12 12-12 12 5.375 12 12z"></path></svg>
    <span>Back to Top</span>
</a>
<?php do_action( 'foundationpress_before_closing_body' ); ?>
</body>
</html>
