<?php
if ( ! function_exists( 'load_more' ) ) :
function load_more( $classes ) {
	if ( in_array( 'sticky', $classes, true ) ) {
	    $classes = array_diff($classes, array('sticky'));
	    $classes[] = 'wp-sticky';
	}
	return $classes;
}
add_filter('post_class','foundationpress_sticky_posts');

endif;