<?php
function loadMetaKeywords() {
    global $wpdb;

    return $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."metakeywords` WHERE `status` = 1 ORDER BY `total` DESC;");
}