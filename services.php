<?php
/* Template Name: ServicesEndpointTemplate */

header('Content-Type: application/json');

class CustomServices
{
  private $limit = 30;
  private $size = 'grid-thumb';

  public function __construct($params = array())
  {
    if (array_key_exists('flag', $params)) {
      switch ($params['flag']) {
        case 'image':
          $this->getImages($params);
          break;
        case 'download':
          $this->download($params);
          break;
        case 'loadmore':
          $this->loadmore($params);
          break;
        case 'image-tag':
          $this->getTopTags($params);
          break;
        default:
          return false;
          break;
      }
    }
  }

  public function getImages($params = array())
  {
    global $wpdb;

    $limit    = array_key_exists('per_page', $params) ? $params['per_page'] : $this->limit;
    $excludes = array_key_exists('exclude', $params) ? explode(',', rtrim(ltrim($params['exclude'], ','), ',')) : array();
    $slots    = implode(',', array_fill(0, count($excludes), '%d'));

    $output = array();

    if (array_key_exists('phrase', $params) && !empty($params['phrase'])) {
      // $terms = strtolower($params['phrase']);
      // $terms = ltrim($terms, ' ');
      // $terms = rtrim($terms, ' ');
      // $terms = explode(' ', $terms);
      //
      // $ambiguous = array('and', 'or', 'the', '&', 'a', 'i', '');
      // $terms = array_diff($terms, $ambiguous);
      //
      // $cues  = implode(',', array_fill(0, count($terms), '%s'));
      //
      // $q = "SELECT
      //   `post_id`
      // FROM
      //   `{$wpdb->prefix}postmeta`
      // WHERE
      //   `meta_key` = '_wp_attachment_metadata'
      // AND
      //   `post_id` NOT IN ({$slots})
      // AND
      //   MATCH(`meta_value`) AGAINST('{$cues}')
      // ORDER BY
      //   RAND()
      // LIMIT
      //   %d;";

      $term = "%".$params['phrase']."%";

      $q = "SELECT
        `post_id`
      FROM
        `".$wpdb->prefix."postmeta`
      WHERE
        `meta_key` = '_wp_attachment_metadata'
      AND
        `post_id` NOT IN (".$slots.")
      AND
        `meta_value` LIKE '%s'
      ORDER BY
        RAND()
      LIMIT
        %d;";

      $params = array($term, $limit);
      $params = array_merge($excludes, $params);

      $IDs = $wpdb->get_results($wpdb->prepare($q, $params));

      if (!empty($IDs)) {
        foreach ($IDs as $id) {
          $q = "SELECT
            *
          FROM
            `".$wpdb->prefix."posts`
          WHERE
            `ID` = %d;";
          $image = $wpdb->get_row($wpdb->prepare($q, $id));

          if (!empty($image)) {
            $output[] = array(
              'id'         => $image->ID,
              'tag'        => wp_get_attachment_image($image->ID, $this->size),
              'preview'    => wp_get_attachment_image_src($image->ID, 'large'),
              'post_title' => $image->post_title,
              'source_url' => $image->guid,
              'permalink'  => wp_get_attachment_url($image->ID),
              'meta'       => $this->getImageMetaData($image->ID)
            );
          }
        }
      } else {
        $output = array('notice' => 'no results found');
      }
    } else {
      $q = "SELECT
        *
      FROM
        `".$wpdb->prefix."posts`
      WHERE
        `post_type` = 'attachment'
      AND
        `post_mime_type` = 'image/jpeg'
      AND
        `ID` NOT IN (".$slots.")
      ORDER BY
        RAND()
      LIMIT
        %d;";

      $params   = $excludes;
      $params[] = $limit;

      $images = $wpdb->get_results($wpdb->prepare($q, $params));

      if (!empty($images)) {
        foreach ($images as $image) {
          $output[] = array(
            'id'         => $image->ID,
            'tag'        => wp_get_attachment_image($image->ID, $this->size),
            'preview'    => reset(wp_get_attachment_image_src($image->ID, 'large')),
            'post_title' => $image->post_title,
            'source_url' => $image->guid,
            'permalink'  => wp_get_attachment_url($image->ID),
            'meta'       => $this->getImageMetaData($image->ID)
          );
        }
      } else {
        $output = array('notice' => 'no results found');
      }
    }

    echo json_encode($output);

    return true;
  }

  public function getPostIDsByCategory($phrase = '', $excludes = array(), $limit = 30)
  {
    global $wpdb;

    $ids    = array();
    $format = implode(', ', array_fill(0, count($excludes), '%d'));
    $params = $excludes;

    if (!empty($phrase)) {
      array_unshift($params, '%'.$phrase.'%');
    }

    $params[] = $limit;

    $q = "SELECT
      `post_id`
    FROM
      `".$wpdb->prefix."postmeta`
    WHERE
      `meta_key` = '_wp_attachment_metadata'".
      (!empty($phrase) ? " AND `meta_value` LIKE '%s' " : '').
    "AND
      `post_id` NOT IN (".$format.")
    ORDER BY
      RAND()
    LIMIT
      %d;";

    $result = $wpdb->get_results($wpdb->prepare($q, $params));

    foreach ($result as $item) {
      if (!in_array($item->post_id, $excludes)) {
        $ids[] = $item->post_id;
      }
    }

    if ($limit - count($ids) > 0) {
      $remaining = $limit - count($ids);
      $cID       = get_cat_ID($phrase);

      if (!empty($cID)) {
        $params = $excludes;
        array_unshift($params, $cID);

        $params[] = $remaining;

        $q = "SELECT
          `object_id`
        FROM
          `".$wpdb->prefix."term_relationships`
        WHERE
          `term_taxonomy_id` = '%d'
        AND
          `object_id` NOT IN (".$format.")
        LIMIT
          %d;";

        $result = $wpdb->get_results($wpdb->prepare($q, $params));

        foreach ($result as $item) {
          if (!in_array($item->object_id, $excludes)) {
            $ids[] = $item->object_id;
          }
        }
      }
    }

    return $ids;
  }

  public function getImageCategories($id = 0)
  {
    global $wpdb;

    $ids = array();

    $q = "SELECT
      `term_taxonomy_id`
    FROM
      `".$wpdb->prefix."term_relationships`
    WHERE
      `object_id` = '%d';";

    return $wpdb->get_results($wpdb->prepare($q, $id));
  }

  public function getImageMetaData($id = 0)
  {
    global $wpdb;

    $q = "SELECT
      `meta_value`
    FROM
      `".$wpdb->prefix."postmeta`
    WHERE
      `post_id` = '%d'
    AND
      `meta_key` = '_wp_attachment_metadata'
    LIMIT
      1;";

    $result = $wpdb->get_row($wpdb->prepare($q, $id));

    return (array) unserialize($result->meta_value);
  }

  public function download($params)
  {
    if (class_exists('ZipArchive')) {
      if (array_key_exists('images', $params) && !empty($params['images'])) {
        $zip      = new ZipArchive();
        $tmp_file = tempnam('.', '');

        $zip->open($tmp_file, ZipArchive::CREATE);

        foreach (explode(',', $params['images']) as $id) {
          $image = wp_get_attachment_image_src($id, 'large');

          if ($file = file_get_contents($image[0])) {
            $zip->addFromString(basename($image[0]), $file);
          }

        }

        $zip->close();

        header('Content-disposition: attachment; filename="igallery-cart_'.time().'.zip"');
        header('Content-type: application/zip');
        readfile($tmp_file);
        unlink($tmp_file);
      }
    }
  }

  public function loadmore($params)
  {
    global $wpdb;

    $values = explode(',', $params['exclude']);
    $format   = implode(', ', array_fill(0, count($values), '%d'));

    if (isset($params['phrase']) && !empty($params['phrase'])) {
      array_unshift($values, '%'.urldecode($params['phrase']).'%');

      $q = "SELECT
        `post_id`
      FROM
        `".$wpdb->prefix."postmeta`
      WHERE
        `meta_key` = '_wp_attachment_metadata'
      AND
        `meta_value` LIKE '%s'
      AND
        `post_id` NOT IN (".$format.")
      LIMIT
        1;";

      $result = $wpdb->get_results($wpdb->prepare($q, $values));
    } else {
      $q = "SELECT
          `post_id`
      FROM
          `".$wpdb->prefix."postmeta`
      WHERE
          `meta_key` = '_wp_attachment_metadata'
      AND
          `post_id` NOT IN (".$format.")
      LIMIT
          1;";

      $result = $wpdb->get_results($wpdb->prepare($q, $values));
    }

    if (!empty($result)) {
      echo true;
    } else {
      echo false;
    }
  }

  public function getTopTags($params)
  {
    global $wpdb;

    if (isset($params['id'])) {
      $q = "SELECT
        `meta_value`
      FROM
        `".$wpdb->prefix."postmeta`
      WHERE
        `meta_key` = '_wp_attachment_metadata'
      AND
        `post_id` = ".$params['id'].";";
    }

    $meta = $wpdb->get_row($q);
    $meta = unserialize($meta->meta_value);
    $meta = $meta['image_meta']['keywords'];

    if (!empty($meta)) {
      $q = "SELECT
        *
      FROM
        `".$wpdb->prefix."metakeywords`
      WHERE
        `keyword` IN ('".implode($meta, "','")."')
      ORDER BY
        `hits` DESC, `total` DESC, `keyword` ASC
      LIMIT
        3;";

      $keywords = $wpdb->get_results($q);

      echo json_encode($keywords);
    } else {
      echo json_encode(array('warning' => 'no meta'));
    }
  }
}

$service = new CustomServices($_GET);
