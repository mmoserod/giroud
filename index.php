<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div id="intro">
	<div class="row">
		<div class="column">
			<h1><a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a></h1>
			<h2><?php bloginfo('description'); ?></h2>
		</div>
	</div>
</div>



<div class="row">
	<div class="column">

		<div id="search">
			<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
			    <label>
			        <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
			        <input type="search" class="search-field"
			            placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>"
			            value="<?php echo get_search_query() ?>" name="s"
			            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
			    </label>
			    <input type="submit" class="search-submit"
			        value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
			</form>
		</div>

		<?php get_template_part( 'template-parts/searchtags', get_post_format() ); ?>


	</div>
</div>




<div class="photos">

	<div id="igallery" >


		<?php
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $args = array(
               'posts_per_page' => 24,
               'post_type' => 'attachment',
               'paged'     => $paged
              );


	      $attachments = get_posts( $args );
	         if ( $attachments ) {
	            foreach ( $attachments as $attachment ) {
	            	$image_attributes = wp_get_attachment_image_src( $attachment->ID, 'grid-thumb' );
	            	echo '<a class="th" ';
	            	echo '" href="';
	            	echo esc_url( home_url( '/' ) ), '?attachment_id='.$attachment->ID;
	            	echo '">';
	            	echo wp_get_attachment_image( $attachment->ID, 'grid-thumb' );
	            	echo '</a>';
	              }
	         }
	     ?>
	</div>

	<?php
	$total_attachments = $wpdb->get_var("SELECT COUNT(ID) FROM {$wpdb->prefix}posts WHERE post_type = 'attachment'");

	$n = (int) round($total_attachments / 10);
	kriesi_pagination($n);
	?>

</div>

<?php get_footer();
