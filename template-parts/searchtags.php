<?php
$q = "SELECT `keyword` FROM `{$wpdb->prefix}metakeywords` WHERE `total` > 1 AND `status` = 1 ORDER BY RAND() LIMIT 5;";
$tags = $wpdb->get_results($q);
?>
<div class="search-tags">
  <p>Popular Searches</p>

  <ul>
    <?php foreach ($tags as $tag) : ?>
      <li>
        <a href="<?php echo home_url(); ?>?s=<?php echo $tag->keyword; ?>" class="hollow button <?php echo (isset($_GET['s']) && $_GET['s'] == $tag->keyword) ? 'selected' : ''; ?>">
          <?php echo $tag->keyword; ?>
        </a>
      </li>
    <?php endforeach; ?>

    <li>
      <button type="button" class="button" data-toggle="offCanvas">
        <i class="fa fa-bars" aria-hidden="true"></i>&nbsp;&nbsp;More
      </button>
    </li>
  </ul>
</div>
