    
    <div class="item">

        <div class="overlay">
            <ul>
                <li class="addtolightbox">
                    <a href="javascript:void(0);" class="add" title="add to lightbox">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </a>
                </li>

                <li class="download">
                    <a href="<?php echo $afiu; ?>" download title="Quick download full version">
                        <i class="fa fa-download" aria-hidden="true"></i>
                    </a>
                </li>
            </ul>
        </div>

        <a class="fancybox" rel="gallery1" href="<?php echo $aliu; ?>" data-title-id="title-<?php echo $attachment->ID; ?>">
            <?php echo wp_get_attachment_image( $attachment->ID, 'large' ); ?>
        </a>

        <div id="title-<?php echo $attachment->ID; ?>" class="hide">
            <a href="<?php echo $attchurl; ?> "><i class="fa fa-info-circle" aria-hidden="true"></i> <?php echo $attachment_title; ?></a>

            <a href="#" class="add" title="add to lightbox">
                <i class="fa fa-plus" aria-hidden="true"></i>
            </a>
            
            <a href="<?php echo $afiu; ?>" download title="Quick download full version">
                <i class="fa fa-download" aria-hidden="true"></i>
            </a>
        </div>
        <!-- <?php echo json_encode($attachment); ?> -->
    </div>
