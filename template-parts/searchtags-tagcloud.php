<ul id="mla_tag_cloud-1" class="tag-cloud tag-cloud-taxonomy-photos_keywords">
    <?php $items = loadMetaKeywords();?>
    <?php foreach($items as $item) :?>
        <li class="tag-cloud-term">
            <a href="<?php echo esc_url(home_url('/'));?>?s=<?php echo $item->keyword;?>" style="font-size: 8pt"><?php echo $item->keyword;?> (<?php echo $item->total;?>)</a>
        </li>
    <?php endforeach;?>
</ul>